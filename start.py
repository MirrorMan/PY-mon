import time
import main
from dungeon_mode import dungeon_mode as DM

def gelp():
    print(" ")
    print("This is help it helps u with basics")
    print("ATTACK: Attack deals damage to the enemy Py-mon, defense absorbs certain amount of damage")
    print("BAG: In a bag you store potions, PARALYZ HEAL, AWAKEN, BURN HEAL, ANTIDOTE ")
    print("POTION 5-15 hp is restored, PARALYZ HEAL removes debuff of paralyz caused by electric py-mon")
    print("AWAKEN removes sleep debuff caused bt fairy pokemon, ANTIDOTE removes poision debuff ")
    print("BURN HEAL burn heal removes burning from fire type py-mon")
    print("------------------------------GAMEPLAY MECHANICS------------------------------")
    print("TACKLE uses fat body to crush ur enemy basic attacks")
    print("PECK uses beak to poke ur enemy in game")
    print("RUN means running away from wild Py-mon and if running away is not succesful ur dead XD")
    print(" ")

def welcome():
    print("welcome to...")
    time.sleep(1)
    print(" _____   __     __           __  __    ____    _   _") 
    print("|  __ \  \ \   / /          |  \/  |  / __ \  | \ | |")
    print("| |__) |  \ \_/ /   ______  | \  / | | |  | | |  \| |")
    print("|  ___/    \   /   |______| | |\/| | | |  | | | . ` |")
    print("| |         | |             | |  | | | |__| | | |\  |")
    print("|_|         |_|             |_|  |_|  \____/  |_| \_|")
    print(" ")
    print("-----------------------------------------------------")
    print("|      BATTLE MODE: {1}     |        HELP: {2}      |")
    print("-----------------------------------------------------")
    print("|      DUNGEON MODE: {3}    |        QUIT: {q}      |")
    print("-----------------------------------------------------")
    print(" ")
    s = input("Please select one: ")
    if s == "1":
        main.menu()
    elif s == "2":
        gelp()
    elif s == "3":
        DM.dun1()
    elif s == "q":
        quit()

welcome()

